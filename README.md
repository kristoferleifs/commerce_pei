### SUMMARY
This module provides a payment method using [PEI](https://www.pei.is) platform.

### INSTALLATION
Using composer:
```
composer require drupal/commerce_pei
```

### CONFIGURATION
- Go to admin/commerce/config/payment-gateways
- Add a new PEI payment gateway
- Edit your gateway settings and credentials.

### SPONSORS
- [Eldum rétt ehf](https://eldumrett.is)

### CONTACT
Developed and maintained by:

1xINTERNET (https://1xinternet.de)

Cambrico (http://cambrico.net).

#### Current maintainers:
- Pedro Cambra [(pcambra)](http://drupal.org/user/122101)
- Manuel Egío [(facine)](http://drupal.org/user/1169056)
- Stefan Weber [(stefanweber)](http://drupal.org/user/137384)