<?php

namespace Drupal\commerce_pei\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_pei\PeiApi;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the PEI payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "pei",
 *   label = "PEI",
 *   display_label = "PEI",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_pei\PluginForm\PeiPaymentMethodAddForm",
 *   },
 *   payment_method_types = {"pei"},
 * )
 */
class OnsitePeiPaymentGateway extends OnsitePaymentGatewayBase implements OnsitePaymentGatewayInterface {

  /**
   * @var PeiApi
   */
  protected $pei;

  /**
   * @var EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new OnsitePeiPaymentGateway object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param PeiApi $pei
   * @param EntityFieldManagerInterface $entityFieldManager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, PeiApi $pei, EntityFieldManagerInterface $entityFieldManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->pei = $pei;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('pei'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'merchant_id' => '',
        'user_id' => '',
        'password' => '',
        'copy_profile_fields' => 0,
        'mapping_issn' => '',
        'mapping_telephone' => '',
      ] + parent::defaultConfiguration();
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('The merchant ID given by PEI for this store'),
      '#default_value' => $this->configuration['merchant_id'],
    ];
    $form['user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User ID'),
      '#description' => $this->t('The user ID given by PEI for this store'),
      '#default_value' => $this->configuration['user_id'],
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('The password given by PEI for this store'),
      '#default_value' => $this->configuration['password'],
    ];
    $form['copy_profile_fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Copy billing profile fields into payment method form as default values'),
      '#description' => $this->t('Allows to pre-fill the form with customer data'),
      '#default_value' => $this->configuration['copy_profile_fields'],
      '#return_value' => 1,
    ];
    // Get the fields that profile of type customer has for mapping.
    $fields = $this->entityFieldManager->getFieldDefinitions('profile', 'customer');
    $profile_fields = [];
    $profile_fields[''] = $this->t('-- Choose profile field --');
    foreach ($fields as $fieldId => $field) {
      $profile_fields[$fieldId] = $field->getLabel();
    }
    // Add the setting for ISSN mapping.
    $form['mapping_issn'] = [
      '#type' => 'select',
      '#default_value' => $this->configuration['mapping_issn'],
      '#title' => $this->t('ISSN field from profile'),
      '#description' => $this->t('Select the field from billing that represents the ISSN'),
      '#options' => $profile_fields,
      '#states' => [
        'visible' => [
          ':input[name="configuration[pei][copy_profile_fields]"]' => ['checked' => TRUE],
        ]
      ],
    ];
    // Add the setting for Telephone mapping.
    $form['mapping_telephone'] = [
      '#type' => 'select',
      '#default_value' => $this->configuration['mapping_telephone'],
      '#title' => $this->t('Telephone field from profile'),
      '#description' => $this->t('Select the field from billing that represents the telephone number'),
      '#options' => $profile_fields,
      '#states' => [
        'visible' => [
          ':input[name="configuration[pei][copy_profile_fields]"]' => ['checked' => TRUE],
        ]
      ],
    ];

    return $form;
  }

  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $merchant_id = $form_state->getValue($form['merchant_id']['#parents']);
    $user_id = $form_state->getValue($form['user_id']['#parents']);
    $password = $form_state->getValue($form['password']['#parents']);

    if (!$this->pei->authenticate($this->configuration['mode'], $merchant_id, $user_id, $password)) {
      $form_state->setError($form, $this->t('Authentication failed.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['user_id'] = $values['user_id'];
      $this->configuration['password'] = $values['password'];
      $this->configuration['copy_profile_fields'] = $values['copy_profile_fields'];
      $this->configuration['mapping_issn'] = $values['mapping_issn'];
      $this->configuration['mapping_telephone'] = $values['mapping_telephone'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    // Authorization is made when ::createPaymentMethod.
    if (!$capture) {
      return;
    }
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $payment_plugin_config = $this->getConfiguration();
    $this->pei->authenticate($this->getMode(), $payment_plugin_config['merchant_id'], $payment_plugin_config['user_id'], $payment_plugin_config['password']);
    $result = $this->pei->submitOrder($payment, $this);
    if (!$result) {
      throw new DeclineException(sprintf('There was an error paying.'));
    }
    $payment->setRemoteId($result);

    $payment->setState('completed');
    $payment->setAuthorizedTime(\Drupal::time()->getRequestTime());
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the PaymentMethodAddForm form elements. They are expected to be valid.
      'telephone', 'issn', 'pincode',
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    // Perform the create request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.

    // We do API requests just in case we are reusing the payment method.
    $payment_plugin_config = $this->getConfiguration();
    $this->pei->authenticate($this->getMode(), $payment_plugin_config['merchant_id'], $payment_plugin_config['user_id'], $payment_plugin_config['password']);

    $issn = $payment_details['issn'];
    $result = $this->pei->checkAccessToBuyer($issn);

    if (!$result) {
      throw new DeclineException(sprintf('kennitala %s is not authenticated.', $issn));
    }

    // Non-reusable payment methods usually have an expiration timestamp.
    $payment_method->telephone = $payment_details['telephone'];
    $payment_method->issn = $payment_details['issn'];
    $payment_method->pincode = $payment_details['pincode'];
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // TODO: Should we do something remotely?
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Delete the local entity.
    $payment_method->delete();
  }

}
