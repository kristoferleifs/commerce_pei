<?php

namespace Drupal\commerce_pei\PluginForm;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm;
use Drupal\commerce_pei\PeiApi;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\profile\Entity\ProfileInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Payment method form for the PEI payment gateway.
 *
 * @package Drupal\commerce_pei\PluginForm
 */
class PeiPaymentMethodAddForm extends PaymentMethodAddForm {

  /**
   * The PEI service.
   *
   * @var \Drupal\commerce_pei\PeiApi
   */
  protected $pei_service;

  /**
   * {@inheritdoc}
   */
  public function __construct(CurrentStoreInterface $current_store, EntityTypeManagerInterface $entity_type_manager, InlineFormManager $inline_form_manager, LoggerInterface $logger, PeiApi $pei_service) {
    parent::__construct($current_store, $entity_type_manager, $inline_form_manager, $logger);
    $this->pei_service = $pei_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_store.current_store'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('logger.channel.commerce_payment'),
      $container->get('pei')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    // Pass billing profile to the form in case it was configured to copy profile fields into form values.
    if (!empty($form['billing_information']['#default_value']) && $form['billing_information']['#default_value'] instanceof ProfileInterface) {
      /** @var ProfileInterface $profile */
      $profile = $form['billing_information']['#default_value'];
      $form_state->set('billing_profile', $profile);
    }
    $form['payment_details'] = $this->buildPeiForm($form['payment_details'], $form_state);
    return $form;
  }

  /**
   * Create the PEI form.
   *
   * @param array $element
   *   The payment details element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The element.
   */
  protected function buildPeiForm(array $element, FormStateInterface $form_state) {
    $wrapper_id = Html::getUniqueId('pei-info-container');

    $element['#prefix'] = '<div id="' . $wrapper_id . '">';
    $element['#suffix'] = '</div>';

    $storage = $form_state->getStorage();

    // Populate the default values.
    $default_issn = '';
    $default_telephone = '';
    // Check if the billing profile was passed.
    if (!empty($storage['billing_profile'])) {
      /** @var ProfileInterface $profile */
      $profile = $storage['billing_profile'];
      // Get the payment gateway settings.
      $pluginConfiguration = $this->plugin->getConfiguration();
      // Check if it was set to copy profile fields.
      if (!empty($pluginConfiguration['copy_profile_fields']) && $pluginConfiguration['copy_profile_fields']) {
        // Get the value for ISSN field if present in profile.
        if (!empty($pluginConfiguration['mapping_issn']) && $profile->hasField($pluginConfiguration['mapping_issn']) && !$profile->get($pluginConfiguration['mapping_issn'])->isEmpty()) {
          $default_issn = $profile->get($pluginConfiguration['mapping_issn'])->value;
        }
        // Get the value for telephone field if present in profile.
        if (!empty($pluginConfiguration['mapping_telephone']) && $profile->hasField($pluginConfiguration['mapping_telephone']) && !$profile->get($pluginConfiguration['mapping_telephone'])->isEmpty()) {
          $default_telephone = $profile->get($pluginConfiguration['mapping_telephone'])->value;
        }
      }
    }

    $element['errors'] = [
      '#type' => 'item',
    ];
    $element['telephone'] = [
      '#type' => 'textfield',
      '#title' => t('Introduce your telephone number'),
      '#default_value' => isset($storage['telephone']) ? $storage['telephone'] : $default_telephone,
      '#access' => !isset($storage['telephone']) && !isset($storage['pincode']),
    ];
    $element['issn'] = [
      '#type' => 'textfield',
      '#title' => t('Introduce your ISSN number'),
      '#default_value' => isset($storage['issn']) ? $storage['issn'] : $default_issn,
      '#access' => !isset($storage['issn']) && !isset($storage['pincode']),
    ];
    $element['pincode'] = [
      '#type' => 'textfield',
      '#title' => t('Introduce the PIN code you just received'),
      '#default_value' => isset($storage['pincode']) ? $storage['pincode'] : '',
      '#access' => !isset($storage['pincode']) && isset($storage['telephone']) && isset($storage['issn']),
    ];

    $element['request_confirmation_submit'] = [
      '#type' => 'submit',
      '#name' => 'ajax-request-confirmation',
      '#value' => t('Request confirmation'),
      '#submit' => [[$this, 'requestAccessToBuyer']],
      '#limit_validation_errors' => [
        array_merge($element['#parents'], ['telephone']),
        array_merge($element['#parents'], ['issn']),
        // If the error is on the API calls, we will set the errors in errors.
        array_merge($element['#parents'], ['errors']),
      ],
      '#ajax' => [
        'callback' => [$this, 'ajaxRefresh'],
        'wrapper' => $wrapper_id,
        'effect' => 'fade',
        'prevent' => 'submit',
        'progress' => [
          'type' => 'throbber',
          'message' => t('Requesting access to buyer...'),
        ],
      ],
      '#access' => !isset($storage['telephone']) && !isset($storage['issn']) && !isset($storage['pincode']),
    ];

    $element['confirm_submit'] = [
      '#type' => 'submit',
      '#name' => 'ajax-confirmation',
      '#value' => t('Confirm'),
      '#submit' => [[$this, 'confirmAccessToBuyer']],
      '#limit_validation_errors' => [
        array_merge($element['#parents'], ['pincode']),
        // If the error is on the API calls, we will set the errors in errors.
        array_merge($element['#parents'], ['errors']),
      ],
      '#ajax' => [
        'callback' => [$this, 'ajaxRefresh'],
        'wrapper' => $wrapper_id,
        'effect' => 'fade',
        'prevent' => 'submit',
        'progress' => [
          'type' => 'throbber',
          'message' => t('Confirming access to buyer...'),
        ],
      ],
      '#access' => !isset($storage['pincode']) && isset($storage['telephone']) && isset($storage['issn']),
    ];

    $element['payment_authorized'] = [
      '#markup' => t('Your payment is authorized. Procceed with the checkout.'),
      '#access' => isset($storage['pincode']) && isset($storage['telephone']) && isset($storage['issn']),
    ];

    return $element;
  }

  /**
   * Refresh the form after an ajax submission.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The payment details element.
   */
  public function ajaxRefresh(array $form, FormStateInterface &$form_state) {
    $submit_element = $form_state->getTriggeringElement();
    $pei_info = NestedArray::getValue($form, array_slice($submit_element['#array_parents'], 0, -1));
    // We show the errors inline.
    $pei_info['errors'] = ['#type' => 'status_messages'];
    return $pei_info;
  }

  /**
   * Validates the payment details.
   *
   * @param array $element
   *   The payment details element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validatePeiForm(array &$element, FormStateInterface $form_state) {
    $telephone = $form_state->getValue($element['telephone']['#array_parents']);
    $issn = $form_state->getValue($element['issn']['#array_parents']);
    $pincode = $form_state->getValue($element['pincode']['#array_parents']);
    if (!is_numeric($telephone)) {
      $form_state->setError($element['telephone'], t('You have entered an invalid telephone number.'));
    }
    if (!is_numeric($issn)) {
      $form_state->setError($element['issn'], t('You have entered an invalid Security Code number.'));
    }
    if (!is_numeric($pincode)) {
      $form_state->setError($element['pincode'], t('You have entered an invalid PIN Code number.'));
    }

    if ($form_state->getErrors()) {
      // We don't need to perform any API calls, already found errors.
      return;
    }

    // We need to the API call methods here, as we cannot set errors in submit handlers.
    $submit_element = $form_state->getTriggeringElement();
    if ($submit_element['#name'] === 'ajax-request-confirmation') {
      $this->doRequestAccessToBuyer($element, $form_state);
    }
    elseif ($submit_element['#name'] === 'ajax-confirmation') {
      $this->doConfirmAccessToBuyer($element, $form_state);
    }

  }

  /**
   * {@inheritdoc}
   *
   * Needed because this is not a paypal or credit card method.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;

    if ($payment_method->bundle() == 'pei') {
      $this->validatePeiForm($form['payment_details'], $form_state);
    }
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * Performs the request access to the PEI service with telephone and issn number.
   *
   * @param array $element
   *   The payment details element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function doRequestAccessToBuyer(array $element, FormStateInterface &$form_state) {
    $errors = FALSE;

    $telephone = $form_state->getValue($element['telephone']['#array_parents']);
    $issn = $form_state->getValue($element['issn']['#array_parents']);

    if (!$errors) {
      $payment_plugin = $this->plugin;
      $payment_plugin_config = $payment_plugin->getConfiguration();
      $this->pei_service->authenticate($payment_plugin->getMode(), $payment_plugin_config['merchant_id'], $payment_plugin_config['user_id'], $payment_plugin_config['password']);
      $access = $this->pei_service->requestAccessToBuyer($issn, $telephone);
      $form_state->setRebuild(TRUE);

      if ($access) {
        $storage = $form_state->getStorage();
        $storage['telephone'] = $telephone;
        $storage['issn'] = $issn;

        if ($access === PeiApi::AUTHENTICATION_GRANTED) {
          // We already were granted. Pincode won't be checked, just put anything.
          $storage['pincode'] = '00000';
        }
        $form_state->setStorage($storage);
      }
      else {
        $form_state->setError($element['errors'], t('Access was not authorized.'));
      }
    }
  }

  /**
   * Performs the request access to the PEI service with telephone and issn number.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function requestAccessToBuyer(array $form, FormStateInterface &$form_state) {
    // We already validated on doRequestAccessToBuyer in validate. No need to do
    // anything else.
  }

  /**
   * Confirms the request access to the PEI service with the PIN  code.
   *
   * @param array $element
   *   The payment details element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function doConfirmAccessToBuyer(array $element, FormStateInterface &$form_state) {
    $pincode = $form_state->getValue($element['pincode']['#array_parents']);

    $storage = $form_state->getStorage();

    $payment_plugin = $this->plugin;
    $payment_plugin_config = $payment_plugin->getConfiguration();
    $this->pei_service->authenticate($payment_plugin->getMode(), $payment_plugin_config['merchant_id'], $payment_plugin_config['user_id'], $payment_plugin_config['password']);
    $access = $this->pei_service->confirmAccessToBuyer($storage['issn'], $pincode);

    if ($access) {
      $form_state->setRebuild(TRUE);
      $storage['pincode'] = $pincode;
      $form_state->setStorage($storage);
    }
    else {
      $form_state->setError($element['errors'], t('Access was not authorized.'));
    }
  }

  /**
   * Confirms the request access to the PEI service with the PIN  code.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function confirmAccessToBuyer(array $form, FormStateInterface &$form_state) {
    // We already validated on doConfirmAccessToBuyer in validate. No need to do
    // anything else.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $submit_element = $form_state->getTriggeringElement();
    if (in_array($submit_element['#name'], ['ajax-request-confirmation', 'ajax-confirmation'])) {
      return;
    }
    parent::submitConfigurationForm($form, $form_state);
  }

}
