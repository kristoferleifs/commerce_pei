<?php

namespace Drupal\Tests\commerce_pei\Unit;

use Drupal\commerce_pei\PeiApi;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @coversDefaultClass \Drupal\commerce_pei\PeiApi
 * @group commerce_pei
 */
class PeiApiUnitTest extends UnitTestCase {

  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $client;

  /**
   * The mock handler for responses.
   *
   * @var \GuzzleHttp\Handler\MockHandler
   */
  protected $mockHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->mockHandler = new MockHandler();
    $handler = HandlerStack::create($this->mockHandler);
    $this->client = new HttpClient(['handler' => $handler]);
  }

  /**
   * ::covers __construct.
   */
  public function testTestEnvironmentIsValid() {
    $api = new PeiApi($this->client, $this->getStringTranslationStub(), '1', 'test');
    $this->assertTrue(TRUE);
  }

  /**
   * ::covers setEnvironment.
   */
  public function testSetterTestEnvironmentIsValid() {
    $api = new PeiApi($this->client, $this->getStringTranslationStub(), '1', 'test');
    $api->setEnvironment('test');
    $this->assertTrue(TRUE);
  }

  /**
   * ::covers authenticate.
   */
  public function testAuthenticateFailsWithAuthorizationDenied() {
    $this->mockHandler->append(new Response(401, [], '{"error":"Authorization has been denied for this request."}'));

    $api = new PeiApi($this->client, $this->getStringTranslationStub());
    $authenticated = $api->authenticate('test', 1, 'user', 'password');
    $this->assertFalse($authenticated);
  }

  /**
   * ::covers authenticate.
   */
  public function testAuthenticateSuccess() {
    $this->mockHandler->append(new Response(200, [], '{"access_token":"my-token","expires_in": 3600,"token_type": "Bearer"}'));

    $api = new PeiApi($this->client, $this->getStringTranslationStub());
    $token = $api->authenticate('test', 1, 'user', 'password');
    $this->assertEquals($token, 'my-token');
  }

  /**
   * ::covers checkAccessToBuyer
   */
  public function testCheckAccessToBuyerSuccess() {
    $this->mockHandler->append(new Response(200, [], 'true'));

    $api = new PeiApi($this->client, $this->getStringTranslationStub(), '1', 'test');
    $access = $api->checkAccessToBuyer('ssn_success');
    $this->assertTrue($access);
  }

  /**
   * ::covers checkAccessToBuyer
   */
  public function testCheckAccessToBuyerFailure() {
    $this->mockHandler->append(new Response(200, [], 'false'));

    $api = new PeiApi($this->client, $this->getStringTranslationStub(), '1', 'test');
    $access = $api->checkAccessToBuyer('ssn_failure');
    $this->assertFalse($access);
  }

  /**
   * ::covers requestAccessToBuyer
   */
  public function testRequestAccessToBuyerSuccess() {
    $this->mockHandler->append(new Response(200, [], 'true'));

    $api = new PeiApi($this->client, $this->getStringTranslationStub(), '1', 'test');
    $access = $api->checkAccessToBuyer('ssn_success');
    $this->assertTrue($access);
  }

  /**
   * ::covers requestAccessToBuyer
   */
  public function testRequestAccessToBuyerFailure() {
    $this->mockHandler->append(new Response(200, [], 'false'));

    $api = new PeiApi($this->client, $this->getStringTranslationStub(), '1', 'test');
    $access = $api->checkAccessToBuyer('ssn_failure');
    $this->assertFalse($access);
  }

}